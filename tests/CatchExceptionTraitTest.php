<?php

declare(strict_types=1);

namespace tests\CatchExceptionTrait;

use Pag\CatchExceptionTrait\CatchExceptionTrait;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class CatchExceptionTraitTest extends TestCase
{
    use CatchExceptionTrait;
    
    public function testThrow(): void
    {
        # Given
        // catchException is the tested method
        
        # When
        $exception = $this->catchException(function() {
            $this->wrapThrow(new RuntimeException('Message', 13));
        });
        
        # Then
        $this->assertEquals(new RuntimeException('Message', 13), $exception);
    }
    
    protected function wrapThrow(RuntimeException $e): void
    {
        throw $e;
    }
    
    public function testNoThrow(): void
    {
        # Given
        // catchException is the tested method
        
        # When
        $exception = $this->catchException(function(){});
        
        # Then
        $this->assertNull($exception);
    }
    
}
