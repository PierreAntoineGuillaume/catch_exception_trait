<?php

declare(strict_types=1);

namespace Pag\CatchExceptionTrait;

trait CatchExceptionTrait
{
    private function catchException(callable $callable): ?\Exception
    {
        try {
            $callable();
            return null;
        } catch (\PHPUnit\Framework\Exception $ex) {
            throw $ex;
        } catch (\Exception $e) {
            return $e;
        }
    }
}
